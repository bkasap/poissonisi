
%% Prolog

close all;
clear;

rng('shuffle');

%% Poisson train parameters

T=1000000;% Total time (in ms)
tau=100; % Mean ISI (in ms)
lambda=1/tau; %Rate

%% Generation method 1
% Generate spike train from a uniform distribution.
% Each time bin, a neuron has the spiking probability of spike_prob_bin.

bin_size=10; % Binsize
bin_number=round(T/bin_size);% Number of bins
spike_prob_bin=lambda*bin_size; % Spiking probability per bin (assumption: bin_size<<tau)

[[?]] % Decide for every bin whether there is a spike or not
spike_times_method1=[[?]] % Compute the times of bins where there was a spike = spike times

%% Generation method 2
% Generate spike train from exponentially distributed inter-spike intervals.
% For each spike, draw the time to the next spike from an exponential distribution.

spike_times_method2=[]; % Initialize spike time vector
last_spike_time=0; % Initialize last spike time

while last_spike_time<=T
[[?]] % Draw new inter spike interval (Exponentially distributed number with rate lambda/mean tau. Alternative: -log(rand)/lambda.)
[[?]] % Compute the new last spike time
[[?]] % Append the new last spike time to the spike time vector
end;
spike_times_method2=[[?]] % Remove the last spike time which is >T

%% Generation method 3
% Generate spike train by drawing N spike times from a uniform distribution over the simulation duration.
% Draw number of spikes for given rate and time from a Poisson distribution.

[[?]]
[[?]]
spike_times_method3=[[?]];

%% Analysis
% Spike trains?

% ISI distributions
ISIs_method1=spike_times_method1(2:end)-spike_times_method1(1:end-1);%Compute ISIs from spike trains for all three methods
ISIs_method2=spike_times_method2(2:end)-spike_times_method2(1:end-1);
ISIs_method3=spike_times_method3(2:end)-spike_times_method3(1:end-1);

bin_size_ISI=10;% Bin size for the ISI histograms
plot_end_ISI=10*tau; % End of plot
x = bin_size_ISI/2:bin_size_ISI:plot_end_ISI-bin_size_ISI/2; %ISI bin centers
exp_distri=lambda.*exp(-lambda.*x).*bin_size_ISI; %Eponential distribution for comparison

ISI_counts_method1=hist(ISIs_method1,x);%Histogram raw data for all three methods
ISI_counts_method2=hist(ISIs_method2,x);
ISI_counts_method3=hist(ISIs_method3,x);

Total_count_method1=sum(ISI_counts_method1);
Total_count_method2=sum(ISI_counts_method2);
Total_count_method3=sum(ISI_counts_method3);

%% Plots

figure()
title('Method 1');
bar(x,ISI_counts_method1/Total_count_method1,'r');
xlabel('ISI');
ylabel('Relative frequency');

figure()
title('Method 2');
bar(x,ISI_counts_method2/Total_count_method2,'g');
xlabel('ISI');
ylabel('Relative frequency');

figure()
title('Method 3');
bar(x,ISI_counts_method3/Total_count_method3,'b');
xlabel('ISI');
ylabel('Relative frequency');

figure()
title('Exp distri');
bar(x,exp_distri,'k');


% All in one for comparison, histograms drawn as curves
figure()
title('All ISI histograms in one plot');
plot(x,ISI_counts_method1/Total_count_method1,'r','LineWidth',2);
hold on;
plot(x,ISI_counts_method2/Total_count_method2,'g','LineWidth',2);
plot(x,ISI_counts_method3/Total_count_method3,'b','LineWidth',2);
plot(x,exp_distri,'k','LineWidth',2);
legend('Method 1','Method 2','Method 3','Exp. distri.');
xlabel('ISI');
ylabel('Relative frequency');






