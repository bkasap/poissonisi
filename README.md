# README #

Matlab and python code to generate spike trains in 3 different methods and illustrating the resulting ISI distributions of these spike trains.

### Includes exercises ###

* Spike train representation as a matrix
* Spike train representation as a list of spiking times
* Introducing array slicing
* Using random distribution libraries for normal, exponential and poisson distributions.
* Plotting ISI distributions

### How do I get set up? ###

* Scripts are independent. Exercise files are provided for Neurophysics students.
* Clone or download the repository and run the files.

### Contribution guidelines ###

* Feedbacks, comments and push requests are welcomed.

### To-do ###

* Spike train plots for matlab files

### Contact ###

* Contact me from repo owner.