import pylab as pl
import numpy as np

## Poisson train parameters

T = 1000000		# Total time (in ms)
tau = 100		# Mean ISI (in ms)
lambd = 1./tau		# Rate

## Generation method 1
# Generate spike train from a uniform distribution.
# Each time bin, a neuron has the spiking probability of spike_prob_bin.

bin_size=10			# Binsize
bin_number=round(T/bin_size)	# Number of bins
spike_prob_bin=lambd*bin_size   # Spiking probability per bin (assumption: bin_size<<tau)

# Decide for every bin whether there is a spike or not
spiketrain_bins=np.random.rand(bin_number)<spike_prob_bin
# Compute the times of bins where there was a spike = spike times
spike_times_method1=pl.find(spiketrain_bins)*bin_size

## Generation method 2
# Generate spike train from exponentially distributed inter-spike intervals.
# For each spike, draw the time to the next spike from an exponential distribution.

spike_times_method2=[]	# Initialize spike time vector
last_spike_time=0	# Initialize last spike time

while last_spike_time<=T:
# Draw new inter spike interval (Exponentially distributed number with rate lambda/mean tau. Alternative: -log(rand)/lambda.)
    ISI=np.random.exponential(tau)
# Compute the new last spike time
    last_spike_time=last_spike_time+ISI
# Append the new last spike time to the spike time vector
    spike_times_method2.append(last_spike_time)

# Remove the last spike time which is >T
spike_times_method2=np.array(spike_times_method2) # list to numpy array
spike_times_method2=spike_times_method2[1:-2]

# Alternative Generation method 2:
ISI = np.random.exponential(tau, T)
strain2 = np.cumsum(ISI)
spike_times_method2 = strain2[strain2<T]

## Generation method 3
# Generate spike train by drawing N spike times from a uniform distribution over the simulation duration.
# Draw number of spikes for given rate and time from a Poisson distribution.

# Draw total number of spikes in [0,T] from Poisson distribution
number_spikes=np.random.poisson(lambd*T)
# Draw spiketimes uniformly over [0,T]
spike_times_method3=np.random.rand(number_spikes)*T
# Sort spiketimes
spike_times_method3=np.sort(spike_times_method3)

## Analysis

# Plot spike trains
pl.figure()
pl.plot(spike_times_method1, np.ones(len(spike_times_method1)), '.')
pl.plot(spike_times_method2, np.ones(len(spike_times_method2))*2, '.')
pl.plot(spike_times_method3, np.ones(len(spike_times_method3))*3, '.')
pl.title("Resulting spike trains")
pl.xlabel("Time (ms)")
pl.ylabel("Spike Train #")
pl.xlim([0,2000])
pl.ylim([-5, 8])

# ISI distributions
# Compute ISIs from spike trains for all three methods
ISIs_method1=spike_times_method1[1:]-spike_times_method1[:-1]
ISIs_method2=spike_times_method2[1:]-spike_times_method2[:-1]
ISIs_method3=spike_times_method3[1:]-spike_times_method3[:-1]

bin_size_ISI=10			# Bin size for the ISI histograms
plot_end_ISI=10*tau		# End of plot
x = np.arange(bin_size_ISI/2, plot_end_ISI-bin_size_ISI/2, bin_size_ISI) # ISI bin centers
exp_distri=lambd*pl.exp(-lambd*x)*bin_size_ISI # Exponential distribution for comparison

# Histogram raw data for all three methods
pl.figure()
ISI_counts_method1=pl.hist(ISIs_method1,x,color='r')
pl.title('Method 1')
pl.xlabel('ISI')
pl.ylabel('Frequency')

pl.figure()
ISI_counts_method2=pl.hist(ISIs_method2,x,color='g')
pl.title('Method 2')
pl.xlabel('ISI')
pl.ylabel('Frequency')

pl.figure()
ISI_counts_method3=pl.hist(ISIs_method3,x,color='b')
pl.title('Method 3')
pl.xlabel('ISI')
pl.ylabel('Frequency')

Total_count_method1=sum(ISI_counts_method1[0])
Total_count_method2=sum(ISI_counts_method2[0])
Total_count_method3=sum(ISI_counts_method3[0])

# All in one for comparison, histograms drawn as curves
pl.figure()
pl.plot(x[:-1],ISI_counts_method1[0]/Total_count_method1,'r',label='Method 1',lw=2)
pl.plot(x[:-1],ISI_counts_method2[0]/Total_count_method2,'g',label='Method 2',lw=2)
pl.plot(x[:-1],ISI_counts_method3[0]/Total_count_method3,'b',label='Method 3',lw=2)
pl.plot(x,exp_distri,'k',label='Exp. distri.',lw=2)
pl.legend()
pl.title('All ISI histograms in one plot')
pl.xlabel('ISI')
pl.ylabel('Relative frequency')
pl.show()




